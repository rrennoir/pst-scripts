#!/bin/bash

cd /home/luca/pst-scripts/bop_notifications_gt2
python3 /home/luca/pst-scripts/bop_notifications_gt2/main.py > bop_notification.log
current_date=$(date +"%Y-%m-%d %H:%M:%S")
git add bop_scores.csv laps_data.csv
git commit -m "new bop data [$current_date]"
