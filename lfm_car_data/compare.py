import json

with open('old.json') as old_file:
  old_data = json.load(old_file)

with open('new.json') as new_file:  
  new_data = json.load(new_file)

  differences = []

  for row1 in old_data:
    name = row1["name"]
    kilometers1 = float(row1["kilometers"])
    laps1 = int(row1["laps"])

    for row2 in new_data:
      if row2["name"] == name:
        kilometers2 = float(row2["kilometers"])
        laps2 = int(row2["laps"])

        diff_kilometers = abs(kilometers1 - kilometers2)
        diff_laps = abs(laps1 - laps2)

        differences.append({
          "name": name,
          "kilometers_diff": diff_kilometers,
          "laps_diff": diff_laps  
        })
        
  differences.sort(key=lambda x: x["kilometers_diff"], reverse=True)

  for diff in differences:
    print(f"{diff['name']}: {diff['kilometers_diff']} km, {diff['laps_diff']} laps")
