# -*- coding: utf-8 -*-

import json
# import urllib library
from urllib.request import urlopen 
import sys
import time
import json
from urllib.request import urlopen
import sys
import time

def time_conversion(sec):
   sec_value = sec % (24 * 3600)
   hour_value = sec_value // 3600
   sec_value %= 3600
   min = sec_value // 60
   sec_value %= 60
   print(str(hour_value) + ":" + str(min) + ":" + str(sec_value))


def Sort(sub_li):
    return(sorted(sub_li, key = lambda x: x[1], reverse=True))

def PruneNDict(d, N):
    return {k: d[k] for k in list(d)[:N]}

def stats():
    race_limit = int(sys.argv[1])
    car_id = int(sys.argv[2])
    url = "https://api2.lowfuelmotorsport.com/api/statistics/safetyrating"
    response = urlopen(url)
    data = json.loads(response.read())
    
    alldata = []
    
    for x in data:
        if(x["races"] >= race_limit):
            alldata.append(x)

    print(len(alldata))
    time.sleep(0.25)
    y = 0
    length = len(alldata)
    car_name = Car_lookup(car_id)
    print(car_name)
    results = []

    for x in alldata:
        time.sleep(0.25)
        y = y + 1
        completed_percent = round(1 - ((length - y) / length), 5) * 100
        print(completed_percent, end='\r')
        name = x["vorname"] + " " + x["nachname"]
        user_id = x["id"]
        
        if len(alldata) < 100:
            print(name + " " + str(user_id))
        
        while True:
            url = f"https://api2.lowfuelmotorsport.com/api/users/getUsersFavouriteCars/{user_id}"
            try:
                response = urlopen(url)
                user_data = json.loads(response.read())
                break
            except json.decoder.JSONDecodeError:
                print("Invalid response, retrying...")
                time.sleep(60)
                continue

        for user_car in user_data:
            if user_car["car_id"] == car_id:
                laps = user_car["laps"]
                kilometers = round(float(user_car["km"]), 3)
                result = {
                    "name": name,
                    "kilometers": kilometers,
                    "laps": laps
                }
                results.append(result)

    # Sort the results by kilometers in descending order
    sorted_results = sorted(results, key=lambda x: x["kilometers"], reverse=True)

    # Save the sorted results to a JSON file with Unicode encoding
    with open(car_name + '.json', 'w', encoding="utf-8") as file:
        json.dump(sorted_results, file, indent=4, ensure_ascii=False)


def Car_lookup(car_id):
    if(car_id == 0):
        return "Porsche 2018"
    if(car_id == 1):
        return "Mercedes 2015"
    if(car_id == 2):
        return "Ferrari"
    if(car_id == 3):
        return "Audi 2015"
    if(car_id == 4):
        return "Lambo 2015"
    if(car_id == 5):
        return "McLaren 650S"
    if(car_id == 6):
        return "Nissan 2018"
    if(car_id == 7):
        return "BMW M6"
    if(car_id == 8):
        return "Bentley 2018"
    if(car_id == 9):
        return "Porsche Cup 2017"
    if(car_id == 10):
        return "Nissan 2015"
    if(car_id == 11):
        return "Bentley 2015"
    if(car_id == 12):
        return "Aston Martin V12"
    if(car_id == 13):
        return "Reiter"
    if(car_id == 14):
        return "Jaguar"
    if(car_id == 15):
        return "Lexus"
    if(car_id == 16):
        return "Lambo Evo"
    if(car_id == 17):
        return "Honda"
    if(car_id == 18):
        return "Lambo ST"
    if(car_id == 19):
        return "Audi Evo"
    if(car_id == 20):
        return "AMR V8"
    if(car_id == 21):
        return "Honda Evo"
    if(car_id == 22):
        return "McLaren 720S"
    if(car_id == 23):
        return "Porsche 2019"
    if(car_id == 24):
        return "Ferrari Evo"
    if(car_id == 25):
        return "Mercedes 2020"
    if(car_id == 26):
        return "Ferrari Challenge"
    if(car_id == 27):
        return "BMW M2"
    if(car_id == 28):
        return "Porsche Cup 2021"
    if(car_id == 29):
        return "Lambo ST Evo 2"
    if(car_id == 30):
        return "BMW M4"
    if(car_id == 31):
        return "Audi Evo 2"
    if(car_id == 32):
        return "Ferrari 296"
    if(car_id == 33):
        return "Lamborghini Evo 2"
    if(car_id == 34):
        return "Porsche 992"
    if(car_id == 35):
        return "McLaren 720S Evo"
    if(car_id == 50):
        return "Alpine"
    if(car_id == 51):
        return "Aston Martin GT4"
    if(car_id == 52):
        return "Audi GT4"
    if(car_id == 53):
        return "BMW GT4"
    if(car_id == 55):
        return "Chevrolet"
    if(car_id == 56):
        return "Ginetta"
    if(car_id == 57):
        return "KTM"
    if(car_id == 58):
        return "Maserati"
    if(car_id == 59):
        return "McLaren 570S"
    if(car_id == 60):
        return "Mercedes GT4"
    if(car_id == 61):
        return "Porsche Cayman"
    if(car_id == 83):
        return "Maserati GT2"
    return ""

def main():
    if(sys.argv[1] == "help" or sys.argv[1] == "--help" or sys.argv[1] == "-help" or len(sys.argv) < 2):
        for x in range(1, 62):
            print(str(x) + " = " + Car_lookup(x))
        print("arg1 minimum races, arg2 car id")
    else:
        stats()

def rank(dictio):
    x = {key: val for key, val in sorted(dictio.items(), key = lambda ele: ele[1], reverse=True)}
    y = {key: rank for rank, key in enumerate(sorted(x, key=x.get, reverse=True), 1)}
    return y

if __name__ == "__main__":
    main()            
