As our resident Formula E enjoyer, Florian is always up to date on the latest in electric racing. We may even get to taste one of his homemade "Tönjeswurst" sausages today.

But jokes aside, Florian brings a great spirit of sportsmanship to our races. Whether battling at the front ~~or fighting further back in the pack,~~ he leaves it all out on the track. We can always count on Florian to drive with respect for his fellow racers.

It's clear motorsport is more than just winning to Florian - it's about enjoying the thrill of competition with friends. We appreciate you for all you contribute to our community both on and off the track. Here's hoping your special day is as exciting as an ePrix race. Thanks for being part of our racing family, <@627201384130805780>! Happy birthday!