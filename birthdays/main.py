import csv
from datetime import datetime
import send_discord_message

def load_birthdays(filename):
    birthdays = {}
    with open(filename, 'r', encoding='utf-8') as file:
        reader = csv.DictReader(file)
        for row in reader:
            #birthdays[(int(row['Month']), int(row['Day']))] = row['Username']
            birthdays[row['Username']] = (int(row['Month']), int(row['Day']))
    return birthdays


def check_birthday(birthdays):
    today = datetime.now().date()
    filtered_birthdays = {username: (month, day) for username, (month, day) in birthdays.items() if (month, day) == (today.month, today.day)}

    for username in filtered_birthdays:    
        print(username)
        try:
            with open(f"{username}.md", 'r') as file:
                birthday_wish = file.read()
                print(birthday_wish)
                send_discord_message.send_discord_message(birthday_wish)
        except FileNotFoundError:
            print(f"No birthday wish found for {username}.")
    else:
        print("No birthdays today.")


def main():
    birthdays = load_birthdays('birthdaybot-1015601496743751740.csv')
    check_birthday(birthdays)


if __name__ == "__main__":
    main()
