import csv
import logging
import random
from datetime import datetime, timedelta
import race
import time
import sys
import os

def write_to_csv(data, file_path = 'races.csv'):
    # Read existing data if the file exists
    existing_data = []
    if os.path.exists(file_path):
        with open(file_path, mode='r', newline='') as file:
            reader = csv.reader(file)
            header = next(reader, None)  # Read header if it exists
            if header:
                existing_data = list(reader)

    # Append new data
    all_data = existing_data + data

    # Sort by date (column index 7 is 'Date and Time')
    all_data.sort(key=lambda x: datetime.strptime(x[7], '%Y-%m-%d %H:%M:%S'))

    # Write sorted data back to the file
    with open(file_path, mode='w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(['Wet Chance', 'Dry Chance', 'Mixed Chance', 'Avg Temp', 'Temp', 'Clouds', 'Rain', 'Date and Time', 'Track', "id", "Event Name"])
        writer.writerows(all_data)

def main():
    weather_data = []
    print(len(sys.argv))
    if len(sys.argv) >= 3:

        # Generate weather data for each hour within the specified range
        races_range = [(int(sys.argv[1]), int(sys.argv[2]) + 1)]

        try:
            for start, end in races_range:
                for race_id in range(start, end):
                    weather_data.append(race.get_race(race_id))
                    time.sleep(1)  # Avoid hitting API limits
        except Exception as e:
            for start, end in races_range:
                for race_id in range(start, end):
                    weather_data.append(race.get_race_small(race_id))
                    time.sleep(1)  # Avoid hitting API limits
            logging.info(f"Using the smaller race info because: {e}")
    
    elif len(sys.argv) < 3:
        try:
            weather_data.append(race.get_race(sys.argv[1]))
        except Exception as e:
            weather_data.append(race.get_race_small(race_id))
            logging.info(f"Using the smaller race info because: {e}")

    

    if len(sys.argv) >= 4:
        # Specify the CSV file path
        csv_file_path = f'{sys.argv[3]}.csv'
            
        # Write data to CSV file (append and sort by date)
        write_to_csv(weather_data, csv_file_path)
    
    else:
        # Write data to CSV file (append and sort by date)
        write_to_csv(weather_data)


if __name__ == "__main__":
    main()
