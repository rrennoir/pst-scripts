import json
import urllib.error
import urllib.request
from datetime import datetime
from dateutil import tz
import emoji
import weathersim

def timestamp(time_string: str) -> int:
    # Convert the string to a datetime object
    date_time_obj = datetime.strptime(time_string, "%Y-%m-%d %H:%M:%S")

    # Convert the datetime object to a Discord timestamp format
    return int(date_time_obj.timestamp()-3600)


def get_race(race_id):
    result = []
    url = f"https://api2.lowfuelmotorsport.com/api/race/{race_id}"
    response = urllib.request.urlopen(url)
    data = json.loads(response.read())
    date_and_time = data['race_date']
        
    track = data['track']['track_name']
    ambient = data['server_settings']['server_settings']['event']['data']['ambientTemp']
    clouds = data['server_settings']['server_settings']['event']['data']['cloudLevel']
    rain = data['server_settings']['server_settings']['event']['data']['rain']
    randomness = data['server_settings']['server_settings']['event']['data']['weatherRandomness']
    hour = data['server_settings']['server_settings']['event']['data']['sessions'][2]['hourOfDay']
    duration = int(data['server_settings']['server_settings']['event']['data']['sessions'][2]['sessionDurationMinutes']) + int(data['server_settings']['server_settings']['event']['data']['sessions'][1]['sessionDurationMinutes']) + 5
    day = data['server_settings']['server_settings']['event']['data']['sessions'][2]['dayOfWeekend']
    event_name = data['event']['event_name']

    result = weathersim.run_simulations(float(rain), float(clouds), int(randomness), int(ambient), int(day), int(hour), int(duration))
    result.append(ambient)
    result.append(clouds)
    result.append(rain)
    result.append(date_and_time)
    result.append(track)
    result.append(race_id)
    result.append(event_name)
    print(result)
    
    return result
    
def get_race_small(race_id):
    result = []
    url = f"https://api2.lowfuelmotorsport.com/api/race/{race_id}"
    response = urllib.request.urlopen(url)
    data = json.loads(response.read())
    date_and_time = data['race_date']
    event_name = data['event']['event_name']

        
    track = data['track']['track_name']    
    result.append(None)
    result.append(None)
    result.append(None)
    result.append(None)
    result.append(None)
    result.append(None)
    result.append(None)
    result.append(date_and_time)
    result.append(track)
    result.append(race_id)
    result.append(event_name)
    print(result)
    
    return result