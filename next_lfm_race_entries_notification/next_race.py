
import csv
from datetime import datetime, timedelta
import sys
import requests
import logging
import send_discord_message
import car_lookup

HOURS_FROM_CET = 0

ids_to_check = [
    1704,
    13457,
    5913,
    5553,
    37158,
    12423,
    7300,
    8077,
    2620,
    22678,
    4909,
    6623,
    39691,
    26240,
    5074,
    38782,
    2771,
    6932,
    2761,
    2774,
    4370,
    12798, # Lyden
    2288,
    44533, # Pran
    2337,
    2661,
    25273, # Ukraine Guy
    40081, # Dink Donk
    109074, # Rosenlund
    6108, # SVXI
]


logging.basicConfig(stream=sys.stdout, level=logging.INFO)

def closest_future_date(date_str, date_list):
    target_date = datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S')
    logging.info(target_date)
    future_dates = [date for date in date_list if datetime.strptime(date, '%Y-%m-%d %H:%M:%S') > target_date]
    logging.info(future_dates[0])
    if future_dates:
        closest_date = min(future_dates, key=lambda x: datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
        return closest_date
    else:
        return None

def get_race_id(check_hour):
    csv_file = 'races.csv'  # Use the single file

    current_datetime = datetime.now()
    if check_hour:
        target_datetime = current_datetime + timedelta(hours=1)  # Define the target as now + 1 hour
    else:
        target_datetime = current_datetime + timedelta(minutes=15)  # Define the target as now + 15 minutes

    # Read and filter data
    with open(csv_file, 'r') as file:
        reader = csv.DictReader(file)
        rows = list(reader)

    # Filter rows for races within the target time frame
    future_rows = []
    for row in rows:
        row_datetime = datetime.strptime(row['Date and Time'], '%Y-%m-%d %H:%M:%S')
        if current_datetime <= row_datetime <= target_datetime:
            logging.info(row)
            future_rows.append(row)

    # Remove rows from the past
    updated_rows = [row for row in rows if datetime.strptime(row['Date and Time'], '%Y-%m-%d %H:%M:%S') >= current_datetime]

    # Overwrite the file with updated rows
    with open(csv_file, 'w', newline='') as file:
        writer = csv.DictWriter(file, fieldnames=reader.fieldnames)
        writer.writeheader()
        writer.writerows(updated_rows)

    if future_rows:
        return future_rows  # Return rows within the target time frame
    else:
        logging.info("No races within the target time frame.")
        return []


def find_drivers(race_id):
    result = ""
    driver_found = False
    url = f"https://api2.lowfuelmotorsport.com/api/race/{race_id}"

    try:
        response = requests.get(url)
        json_data = response.json()

        if 'participants' in json_data:
            entrylist = json_data['participants']
            if 'entries' in entrylist:
                entries = entrylist['entries']
                for entry_info in entries:
#                    driver = entry_info['drivers'][0]
                    first_name = entry_info['vorname']
                    last_name = entry_info['nachname']
                    raceNumber = f"[{entry_info['raceNumber']}]"
                    car_id = entry_info['car_model']
                    id = entry_info['user_id']
                    car_name = f"({car_lookup.Car_lookup(car_id)})"
                    name = f"{first_name} {last_name}"
                    if id in ids_to_check:
                        result += (f"{name.ljust(30)} {car_name.ljust(30)} {raceNumber}\n")
                        driver_found = True
                    else:
                        logging.info(f"Driver {first_name} {last_name} is not in the provided list.")
                if driver_found:
                    result = f"```\n{result}```"
            else:
                logging.info("No entries found for this race.")
        else:
            logging.info("No entrylist found in the JSON data.")

    except Exception as e:
        logging.info(f"An error occurred: {e}")
    return f"{result}"


def format_discord_message(row, series):
    try:
        track = row.get('Track', 'Unknown Track')
        wet_chance = float(row.get('Wet Chance'))*100 if row.get('Wet Chance') != '' else "N/A"
        dry_chance = float(row.get('Dry Chance'))*100 if row.get('Dry Chance') != '' else "N/A"
        mixed_chance = float(row.get('Mixed Chance'))*100 if row.get('Mixed Chance') != '' else "N/A"
        avg_temp = float(row.get('Avg Temp')) if row.get('Avg Temp') != '' else "N/A"

        if wet_chance != "N/A":
            message = (
                f"**{series.upper()} Race Alert**\n"
                f"Track: {track}\n"
                f"Conditions: Wet: {wet_chance:.2f}%, Dry: {dry_chance:.2f}%, Mixed: {mixed_chance:.2f}%\n"
                f"Average Temperature: {avg_temp}°C"
            )
        else:
            message = (
                f"**{series.upper()} Race Alert:\n**"
                f"Track: {track}\n"
            )
    except Exception as e:
        logging.error(f"Error formatting message: {e}")
        message = "Race information could not be processed."

    return message


def main():
    hour = sys.argv[1]

    if hour.lower() == 'true':
        rows = get_race_id(True)
    else:
        rows = get_race_id(False)

    for row in rows:
        race_id = row['id']
        logging.info(race_id)
        result = find_drivers(race_id)
        if result != "":
            print(row)
            series = row['Event Name']
            message = format_discord_message(row, series)
            message += "\n"
            message += find_drivers(race_id)
            logging.info("#####")
            logging.info("\n")
            logging.info(message)
            send_discord_message.send_discord_message(message)

if __name__ == "__main__":
    main()

