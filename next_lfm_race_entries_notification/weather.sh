#!/bin/bash

echo "GT3 Series"
python3 main.py 165498 166505

echo "Sprint Series"
python3 main.py 167514 169529

echo "Rookie Series"
python3 main.py 166506 167513

echo "GT3 Rookie Series"
python3 main.py 169530 171545

echo "GT4 Series"
python3 main.py 171546 172049

echo "Single Make Series"
python3 main.py 172074 172493

echo "Duo Cup"
python3 main.py 172056 172061

echo "Endurance Series"
python3 main.py 172062 172073

echo "AMS2"

python3 main.py 176628 177635
python3 main.py 177636 178643
python3 main.py 178644 179651
python3 main.py 179652 181667
python3 main.py 181668 183683
python3 main.py 183684 184019
python3 main.py 184020 184355
python3 main.py 184356 184367