import bop
import time
import logging
import os
import send_discord_message


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
LFM_API = "https://api2.lowfuelmotorsport.com/api"

# Specify the file name
csv_file_name = 'laps_data.csv'
temp_file = "temp.csv"

# Specify the header names
header = ['car_id', 'track_id', 'car_name', 'lap', 'bop', 'bop_raw', 'source']

cars_to_monitor = ['Maserati MC GT4']

track_list = bop.get_all_tracks()
for track in track_list:
    result = ""
    time.sleep(5)
    track_id = bop.get_track_id(track)
    relevant_laps, others_laps = bop.get_bop(track)
    
    for lap in relevant_laps:
        car_id = lap.get('car_id')
        car = lap.get('car_name')
        try:
            old_bop = bop.search_data(str(car_id), str(track_id)).get('bop')
            new_bop = lap.get('bop')
            difference = int(new_bop) - int(old_bop)

            if int(old_bop) != int(new_bop):
                # insert discord message
                logger.info(f"{car} BOP changed from {old_bop} to {new_bop} on {track}")
                if difference > 0:
                    result += f"- {car} BOP changed from {old_bop} to {new_bop} [Δ{difference}] on {track}\n"
                elif difference < 0:
                    result += f"+ {car} BOP changed from {old_bop} to {new_bop} [Δ{difference}] on {track}\n"

            else:
                logger.info(f"### {car} BOP changed from {old_bop} to {new_bop} on {track}")
        
        except:  # noqa: E722
            logger.info(f"BOP Search didn't find car {car_id}")

    for lap in others_laps:
        car_id = lap.get('car_id')
        car = lap.get('car_name')

        if car in cars_to_monitor:
            try:
                old_bop =  bop.search_data(str(car_id), str(track_id)).get('bop')
                new_bop = lap.get('bop')

                if int(old_bop) != int(new_bop):
                    # insert discord message
                    logger.info(f"{car} BOP changed from {old_bop} to {new_bop} on {track}")
                    if difference > 0:
                        result += f"- {car} BOP changed from {old_bop} to {new_bop} [Δ{difference}] on {track}\n"
                    elif difference < 0:
                        result += f"+ {car} BOP changed from {old_bop} to {new_bop} [Δ{difference}] on {track}\n"
                else:
                    logger.info(f"### {car} BOP changed from {old_bop} to {new_bop} on {track}")
            except:  # noqa: E722
                logger.info(f"BOP Search didn't find car {car_id}")


    if result != "":
        #send_discord_message.send_discord_message(f"```diff\n{result}\n```")
        logger.info(result)

    bop.write_bop_to_csv(relevant_laps, others_laps, track_id, temp_file)            


os.remove(csv_file_name)
os.rename(temp_file, csv_file_name)