import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import sys
import os
from dotenv import load_dotenv

# Load variables from .env file
load_dotenv()

# Get the # Gmail credentials from environment variables
app_password = os.getenv("app_password")
sender_email = os.getenv("sender_email")
receiver_email = os.getenv("receiver_email")


# Email content
subject = "Test Email"
if len(sys.argv) > 1:
    body = sys.argv[1]
else:
    print("No input.")
    exit

# Create the email
msg = MIMEMultipart()
msg['From'] = sender_email
msg['To'] = sender_email
msg['Subject'] = subject
msg.attach(MIMEText(body, 'plain'))

print(msg)

try:
    # Connect to Gmail's SMTP server
    with smtplib.SMTP("smtp.gmail.com", 587) as server:
        server.starttls()  # Upgrade the connection to secure
        server.login(sender_email, app_password)
        server.sendmail(sender_email, receiver_email, msg.as_string())
        print("Email sent successfully!")
except Exception as e:
    print(f"Failed to send email: {e}")
