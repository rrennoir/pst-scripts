from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route('/process', methods=['POST'])
def process_data():
    data = request.json['data']
    # Process data here
    result = data[::-1]  # Example: reverses string data
    return jsonify(result=result)

if __name__ == "__main__":
    app.run(port=5000)
