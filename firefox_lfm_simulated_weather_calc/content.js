document.querySelector("#your-button").addEventListener("click", () => {
  const data = document.querySelector("#data-element").innerText;
  chrome.runtime.sendMessage(
    { action: "processData", data },
    (response) => {
      document.querySelector("#output-element").innerText = response.result;
    }
  );
});
