async function sendDataToPython(data) {
  const response = await fetch('http://localhost:5000/process', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ data })
  });
  const result = await response.json();
  return result.result;
}

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  if (request.action === "processData") {
    sendDataToPython(request.data).then(response => {
      sendResponse({ result: response });
    });
    return true;  // Keep the message channel open
  }
});
