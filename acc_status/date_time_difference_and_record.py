from datetime import datetime

# Save current date and time to a file
def save_current_datetime(file_path):
    current_time = datetime.now().isoformat()
    with open(file_path, 'w') as file:
        file.write(current_time)

# Get date and time from a file
def get_datetime_from_file(file_path):
    try:
        with open(file_path, 'r') as file:
            return datetime.fromisoformat(file.read().strip())
    except FileNotFoundError:
        return None

# Calculate the difference between two datetime objects
def calculate_difference(start_datetime, end_datetime):
    return (end_datetime - start_datetime).total_seconds()

# Save a time difference to a file
def save_difference(file_path, difference):
    with open(file_path, 'w') as file:
        file.write(str(difference))

# Get a time difference from a file
def get_difference_from_file(file_path):
    try:
        with open(file_path, 'r') as file:
            return float(file.read().strip())
    except FileNotFoundError:
        return None

# Update the difference in the file if the new difference is larger
def update_difference_if_larger(file_path, new_difference):
    current_difference = get_difference_from_file(file_path)
    if current_difference is None or new_difference > current_difference:
        save_difference(file_path, new_difference)
        return True
    return False

def convert_difference_to_readable_format(seconds):
    # Constants for conversion
    year_seconds = 31556952  # Average year length accounting for leap years
    month_seconds = 2629746  # Average month length (30.44 days)
    day_seconds = 86400
    hour_seconds = 3600
    minute_seconds = 60

    # Calculate each time unit
    years, seconds = divmod(seconds, year_seconds)
    months, seconds = divmod(seconds, month_seconds)
    days, seconds = divmod(seconds, day_seconds)
    hours, seconds = divmod(seconds, hour_seconds)
    minutes, seconds = divmod(seconds, minute_seconds)

    # Build the readable string
    parts = []
    if years:
        parts.append(f"{int(years)}y")
    if months:
        parts.append(f"{int(months)}m")
    if days:
        parts.append(f"{int(days)}d")
    if hours:
        parts.append(f"{int(hours)}h")
    if minutes:
        parts.append(f"{int(minutes)}min")
    if seconds:
        parts.append(f"{int(seconds)}sec")

    return " ".join(parts)

# # Example usage
# datetime_file = 'datetime.txt'
# difference_file = 'difference.txt'

# # Retrieve datetime and calculate difference with now
# saved_datetime = get_datetime_from_file(datetime_file)

# # Save current datetime
# save_current_datetime(datetime_file)

# if saved_datetime:
#     new_difference = calculate_difference(saved_datetime, datetime.now())
    
#     # Update the difference file if the new difference is larger
#     if update_difference_if_larger(difference_file, new_difference):
#         print("Difference file updated.")
#     else:
#         print("Difference file not updated.")
