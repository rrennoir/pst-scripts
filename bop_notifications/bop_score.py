import bop
import pandas as pd
import logging
import requests
import csv

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def get_bop(track):
        
    # URL to fetch JSON data from
    url = f'https://api2.lowfuelmotorsport.com/api/hotlaps/getBopPrediction?track={bop.get_track_id(track)}'
    logger.info(url)
    try:
        # Send an HTTP GET request to the URL and parse the JSON response
        data = requests.get(url).json()

        # Check if the JSON data has the expected structure
        if 'laps_relevant' in data and 'laps_others' in data:
            relevant_laps = data['laps_relevant']
            others_laps = data['laps_others']

            # Convert the relevant laps data to a Pandas DataFrame
            relevant_df = pd.DataFrame(relevant_laps)
            # Add a "relevant" column and set it to True
            relevant_df['relevant'] = 'X'

            # Convert the others laps data to a Pandas DataFrame
            others_df = pd.DataFrame(others_laps)
            # Add a "relevant" column and set it to an empty string
            others_df['relevant'] = ''

            # Sort the others DataFrame by "bop_raw" in descending order
            others_df = others_df.sort_values(by='bop_raw', ascending=False)

            # Combine both DataFrames into one
            combined_df = pd.concat([relevant_df, others_df])
            combined_df = combined_df.sort_values(by='bop_raw', ascending=False)

            # Reorder columns
            combined_df = combined_df[['car_name', 'car_year', 'target_dif', 'lap', 'bop', 'bop_raw', 'relevant']]
            # Reset the row numbering to start from 1
            combined_df.reset_index(drop=True, inplace=True)
            # Add 1 to the index to start from 1
            combined_df.index += 1

            # Print the combined DataFrame
            return combined_df
        else:
            print('JSON data does not have the expected structure.')
    except Exception as e:
        print(f'An error occurred: {e}')


def get_score(track):
    if "nordschleife" in track.lower():
        track = "Nordschleife"
    track_bop = get_bop(track)
    lap_values = track_bop.loc[track_bop['relevant'] == 'X', 'lap'].tolist()
    bop_values = track_bop.loc[track_bop['relevant'] == 'X', 'bop'].tolist()
    for x in range(len(bop_values)):
        bop_values[x] = int(bop_values[x])

    average_bop = (sum(bop_values)/len(bop_values))

    for x in range(len(lap_values)):
        lap_values[x] = float(bop.time_to_seconds(lap_values[x]))

    result = track_bop
    result = result[(result['relevant'] == 'X')]

    difference = float(str(result['target_dif'].iloc[0])[4:])
    kg = float(str(result['bop'].iloc[0])[-3:])

    kg = round(difference/kg, 4)

    #kg = (bop.get_kg_per_second(track))
    average_lap = (sum(lap_values)/len(lap_values))
    overflow = (average_lap - lap_values[0]) / kg
    lap_time_diff = lap_values[0] - lap_values[len(lap_values)-1]

    score = (100 + (average_bop) - overflow - int(bop_values[0]) - lap_time_diff)/10 + 40/int(bop_values[0])
    logger.info(f"(100 + {average_bop} - {overflow} - {bop_values[0]} - {lap_time_diff})/10 + 40/{bop_values[0]}")
    # (100 + (-0.1) - 14 - 14 - 0.699)/10 + 40/14
    score = round(score, 3)
    return min(max(score, 0.0), 10.0)

def get_all():
    return_string = ""
    track_score_dict = {}
    adjust = 40

    track_list = bop.get_all_tracks()
    # Calculate the score for each track and store it in the dictionary
    for track in track_list:
        print(track)
        score = get_score(track)
        track_score_dict[track] = round(score, 3)

    # Sort the dictionary by scores in descending order
    sorted_track_score = dict(sorted(track_score_dict.items(), key=lambda item: item[1], reverse=True))

    # Print the sorted results
    for track, score in sorted_track_score.items():
        return_string += (f"{track.ljust(adjust)}: {score}\n")

    return return_string

def find_bop_score(track):
    with open("bop_scores.csv", mode='r') as file:
            csv_reader = csv.DictReader(file)

            # Assuming 'car_id' and 'track_id' are the names of the columns containing car and track IDs
            for row in csv_reader:
                track_id = row.get('track_id')
                bop_score = row.get('bop_score')
                #print(f"{track_id} {bop_score}")
                if str(track_id) == str(track):
                   return bop_score

            logger.info(f'Entry with track_id {track} not found.')