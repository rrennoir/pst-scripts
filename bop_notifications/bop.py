import requests
import pandas as pd
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import logging
import json
import datetime
import csv
import os
import time


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
LFM_API = "https://api2.lowfuelmotorsport.com/api"

# Specify the file name
csv_file_name = 'laps_data.csv'
temp_file = "temp.csv"

# Specify the header names
header = ['car_id', 'track_id', 'car_name', 'lap', 'bop', 'bop_raw', 'source']

cars_to_monitor = ['Ferrari 488 GT3 Evo']

def get_track_id(track_name):

    track_list_response = requests.get(f"{LFM_API}/lists/getTracks")

    # Check if the request was successful (status code 200)
    if track_list_response.status_code != 200:
        logger.warn("Failed to get list of track IDs from LFM API")
        return None

    data = track_list_response.json()

    # Filter tracks with track_id between 124 and 155
    filtered_tracks = [track for track in data if track['track_id'] == 249 or 124 <= track['track_id'] <= 155 or track['track_id'] == 223]

    # Create a dictionary with track_name as keys and track_id as values
    track_dict = {track['track_name']: track['track_id'] for track in filtered_tracks}

    # Convert track_dict keys to a list of strings
    track_list = list(track_dict.keys())

    # Find the closest matching track name
    closest_track = find_closest_track(track_list, track_name)

    # Get the corresponding track_id
    track_id = track_dict.get(closest_track)

    return track_id


def find_closest_track(match_tracks, input_track):
    closest_match, _ = process.extractOne(input_track, match_tracks, scorer=fuzz.partial_ratio)
    return closest_match


def get_bop(track):
    if "nordschleife" in track.lower():
        track = "nordschleife"
    track_id = get_track_id(track)
    
        
    # URL to fetch JSON data from
    url = f'https://api2.lowfuelmotorsport.com/api/hotlaps/getBopPrediction?track={track_id}'
    logger.info(url)
    try:
        # Send an HTTP GET request to the URL and parse the JSON response
        data = requests.get(url).json()

        # Check if the JSON data has the expected structure
        if 'laps_relevant' in data and 'laps_others' in data:
            relevant_laps = data['laps_relevant']
            others_laps = data['laps_others']

        
    except Exception as e:
        print(f'An error occurred: {e}')
    
    #logger.info(relevant_laps)
    
    return (relevant_laps, others_laps)

def write_bop_to_csv(relevant_laps, others_laps, track_id, csv_file_name):

     with open(csv_file_name, mode='a', newline='') as file:
        # Create a CSV writer object
        csv_writer = csv.writer(file)

        for lap in relevant_laps:
            #logger.info(lap)
            car_id = lap.get('car_id')
            car_name = lap.get('car_name')
            lap_time = lap.get('lap')
            bop = lap.get('bop')
            bop_raw = lap.get('bop_raw')
            source = lap.get('source')
        
            # Write the data to the CSV file
            csv_writer.writerow([car_id, track_id, car_name, lap_time, bop, bop_raw, source])

        for lap in others_laps:
            #logger.info(lap)
            car_id = lap.get('car_id')
            car_name = lap.get('car_name')
            lap_time = lap.get('lap')
            bop = lap.get('bop')
            bop_raw = lap.get('bop_raw')
            source = lap.get('source')
        
            # Write the data to the CSV file
            if int(bop) > 0:
                csv_writer.writerow([car_id, track_id, car_name, lap_time, bop, bop_raw, source])

def search_data(target_car_id, target_track_id, csv_file = csv_file_name):
    with open(csv_file, mode='r') as file:
        csv_reader = csv.DictReader(file)

        # Assuming 'car_id' and 'track_id' are the names of the columns containing car and track IDs
        for row in csv_reader:
            car_id = row.get('car_id')
            track_id = row.get('track_id')
            if car_id == target_car_id and track_id == target_track_id:
                print(f'Entry found:\n{row}')
                return row

        logger.info(f'Entry with car_id {target_car_id} and track_id {target_track_id} not found.')


def time_to_seconds(time_str):
    # Split the time string into minutes, seconds, and milliseconds
    minutes, seconds = time_str.split(':')
    
    # Calculate the total time in seconds
    total_seconds = int(minutes) * 60 + float(seconds)
    
    return total_seconds


def float_to_time(float_value):
    minutes, seconds = divmod(float_value, 60)
    return f"{int(minutes):02d}:{seconds:06.3f}".lstrip("0")


def get_all_tracks():

    # Make a GET request to the API
    response = requests.get(f"{LFM_API}/lists/getTracks")

    # Check if the request was successful (status code 200)
    if response.status_code != 200:
        logger.warning("Failed to get track ID from LFM API")
        return None

    # Parse the JSON response
    data = response.json()

    # Filter tracks with track_id between 124 and 155
    filtered_tracks = [track for track in data if track['track_id'] == 249 or 124 <= track['track_id'] <= 155 or track['track_id'] == 223]

    # Create a dictionary with track_name as keys and track_id as values
    track_dict = {track['track_name']: track['track_id'] for track in filtered_tracks}

    # Convert track_dict keys to a list of strings
    track_list = list(track_dict.keys())

    return track_list

def get_all_bop():
    track_list = get_all_tracks()
    car_list = [33, 32, 31, 35, 25, 20, 8, 30, 34, 21, 24]

    output_file_path = "output.csv"  # Replace with the desired file path

    # Open the file in write mode
    with open(output_file_path, 'w') as output_file:
    
        output_file.write("car_id,track_id,car_name,lap,bop,bop_raw,source\n")

        for track in track_list:
            data = get_bop(track)
            for car in car_list:
                #logger.info(data)
                
                # Flag to check if the car is found in any lap
                car_found = False
                
                for lap in data[0]:
                    #logger.info(lap)
                    if lap['car_id'] == car:
                        temp = f"{car},{get_track_id(track)},{lap['car_name']},{lap['lap']},{lap['bop']},{lap['bop_raw']},{lap['source']}"
                        logger.info(temp)
                        output_file.write(f"{temp}\n")
                        car_found = True  # Set the flag to True if the car is found in any lap
                
                for lap in data[1]:
                    logger.info(lap)
                    if lap['car_id'] == car:
                        temp = f"{car},{get_track_id(track)},{lap['car_name']},{lap['lap']},{lap['bop']},{lap['bop_raw']},{lap['source']}"
                        logger.info(temp)
                        output_file.write(f"{temp}\n")
                        car_found = True  # Set the flag to True if the car is found in any lap
                
                # Check if the car was not found in any lap
                if not car_found:
                    temp = f"{car},{get_track_id(track)},Car with id {car},-1,-40,-40,unknown_source"
                    logger.info(temp)
                    output_file.write(f"{temp}\n")
