import json
import csv
import requests  # Assuming you have the 'requests' library installed
import os
import sys
from datetime import datetime, timedelta
import time
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def fetch_data_from_url(url):
    response = requests.get(url)
    response.raise_for_status()

    try:
        # Check if the response is a JSON object
        data = response.json()
    except json.JSONDecodeError:
        # If not, assume the response is already in dictionary format
        data = response

    return data


def parse_time(time_str):
    try:
        # Parse the time string in the format minute:seconds.milliseconds
        parts = time_str.split(':')
        minutes = int(parts[0])
        seconds = float(parts[1])
        return timedelta(minutes=minutes, seconds=seconds)
    except ValueError:
        return timedelta(0)

# Specify the path for the CSV file
csv_file_path = 'race_results.csv'

# Specify the range of IDs (73397 to 73401 in this case)
id = sys.argv[1]
#ids = list(range(73397, 73409))

# Specify the CSV headers based on the selected fields
csv_headers = [
    "season_week", "vorname", "nachname", "origin", "user_id",
    "car_name", "position", "bestlap", "time", "time_penalty",
    "dnf", "points", "gap", "dns", "dsq", "car_number", "bestOfTheWeek",
    "track_name", "competition", "pole_position", "fastest_lap", "starting_position", "sof", "ratingGain", "sr_change", "incidents", "race_date", "race_id"  # Added new fields for track name and server name
]

# Open the CSV file in append mode
with open(csv_file_path, 'a', newline='') as csv_file:
    csv_writer = csv.DictWriter(csv_file, fieldnames=csv_headers)

    # Uncomment the following line if you want to write headers only once
    # csv_writer.writeheader()
    is_csv_empty = not os.path.exists(csv_file_path) or os.path.getsize(csv_file_path) == 0

    # Write headers only if the CSV file is empty
    if is_csv_empty:
        csv_writer.writeheader()


    # Construct the URL for each ID
    url = f"https://api2.lowfuelmotorsport.com/api/race/{id}"

    POSSIBLE_CAR_CLASSES = ["GT3", "TCX", "CUP"]

    try:
        # Make a request to the API or load the data from the file
        data = fetch_data_from_url(url)

        # Extract race results from the JSON data
        race_results = []
        counter = 0
        while len(race_results) == 0:
            race_results = data.get("race_results", {}).get(POSSIBLE_CAR_CLASSES[counter], {}).get("OVERALL", [])
            counter += 1
    

        # Extract track name and server name from the API response
        track_name = data.get("track", {}).get("track_name", "N/A")
        competition = data.get("event", {}).get("url_code", "N/A")
        race_date = data.get("race_date", {})

        # Extract quali results and check for pole position
        quali_results = data.get("quali_results", [])
        pole_position_user_id = quali_results[0]["user_id"] if quali_results else None

        # Find the fastest lap time among all drivers
        zero_time = parse_time("0:00:00")
        fastest_lap_time = min(parse_time(result["bestlap"]) for result in race_results if parse_time(result["bestlap"]) != zero_time)


        SOF = data.get("sof", {})


        # Write each race result entry to the CSV file
        for result in race_results:
            selected_fields = {key: result[key] for key in csv_headers[:-11]}  # Exclude new fields
            selected_fields["track_name"] = track_name
            selected_fields["competition"] = competition
            selected_fields["race_date"] = race_date
            selected_fields["race_id"] = id
            selected_fields["pole_position"] = result["user_id"] == pole_position_user_id

            for x, y in enumerate(quali_results):
                if y.get('user_id') == result['user_id']:
                    selected_fields["starting_position"] = x+1
                    break  # Stop the loop once a match is found


            # Check if the current driver has the fastest lap
            current_lap_time = parse_time(result["bestlap"])
            selected_fields["fastest_lap"] = current_lap_time == fastest_lap_time
            selected_fields['sof'] = SOF
            selected_fields['ratingGain'] = result['ratingGain']
            selected_fields['sr_change'] = result['sr_change']
            selected_fields['incidents'] = result['incidents']
            csv_writer.writerow(selected_fields)

        print(f"Race results for ID {id} have been appended to {csv_file_path}")
        time.sleep(0.1)
    except Exception as e:
        print(f"Error fetching data for ID {id}: {e}")
