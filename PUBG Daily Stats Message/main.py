import requests
import json
from datetime import datetime, timedelta
import time
import send_discord_message
import logging
import os
from dotenv import load_dotenv


logging.basicConfig(level=logging.INFO, format="[%(asctime)s] %(name)s: %(message)s", datefmt="%H:%M:%S")
logger = logging.getLogger(__name__)

logger.info("Loading env variables")
load_dotenv()
TOKEN = os.getenv("TOKEN")

# Function to fetch match data for a given match ID
def fetch_match_data(match_id):
    api_key = TOKEN  # Replace with your PUBG API key
    url = f"https://api.pubg.com/shards/steam/matches/{match_id}"
    headers = {
        "Authorization": f"Bearer {api_key}",
        "Accept": "application/vnd.api+json"
    }
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        data = response.json()
        return data
    else:
        print(f"Failed to fetch match data for match ID {match_id}. Status code: {response.status_code}")
        return None

# Function to convert time in seconds to minutes and seconds
def seconds_to_time(seconds):
    minutes = seconds // 60
    remaining_seconds = seconds % 60
    return f"{minutes} minutes {remaining_seconds} seconds"

# Function to calculate average and maximum statistics from match data
def calculate_stats(matches_data, player_name):
    counter = 0
    total_matches = len(matches_data)
    total_damaged = 0
    total_boosts = 0
    total_headshots = 0
    total_revives = 0
    total_downed = 0
    total_time_survived = 0
    total_ride_distance = 0
    total_walk_distance = 0
    max_longest_kill = 0
    total_win_place = 0
    wins = 0

    for match_data in matches_data:
        counter += 1
        included = match_data.get("included", [])
        for player in included:
            if player["type"] == "participant" and player["attributes"]["stats"]["name"] == player_name:
                
                attributes = player['attributes']['stats']
                print(attributes)

                total_damaged += attributes.get('damageDealt', 0)
                total_boosts += attributes.get('boosts', 0)
                total_headshots += attributes.get('headshotKills', 0)
                total_revives += attributes.get('revives', 0)
                total_downed += attributes.get('DBNOs', 0)
                total_time_survived += attributes.get('timeSurvived', 0)
                total_ride_distance += attributes.get('rideDistance', 0)
                total_walk_distance += attributes.get('walkDistance', 0)
                max_longest_kill = max(max_longest_kill, attributes.get('longestKill', 0))
                total_win_place += attributes.get('winPlace', 0)
                if attributes.get('winPlace', 0) == 1:
                    wins += 1


    average_damaged = round(total_damaged / total_matches, 3)
    average_boosts = round(total_boosts / total_matches, 3)
    average_headshots = round(total_headshots / total_matches, 3)
    average_revives = round(total_revives / total_matches, 3)
    average_downed = round(total_downed / total_matches, 3)
    average_time_survived = seconds_to_time(total_time_survived // total_matches)
    average_ride_distance = round(total_ride_distance / total_matches, 3)
    average_walk_distance = round(total_walk_distance / total_matches, 3)
    max_longest_kill = round(max_longest_kill, 3)
    average_win_place = round(total_win_place / total_matches, 3)

    return {
        "average_damaged": average_damaged,
        "average_boosts": average_boosts,
        "average_headshots": average_headshots,
        "average_revives": average_revives,
        "average_downed": average_downed,
        "average_time_survived": average_time_survived,
        "average_ride_distance": average_ride_distance,
        "average_walk_distance": average_walk_distance,
        "max_longest_kill": max_longest_kill,
        "average_win_place": average_win_place,
        "total_matches": total_matches,
        "wins": wins
    }

def main():
    player_names = ["Temetias", "Haunted_1", "KeertKRAKAKA", "Riba_xd"]
    api_key = TOKEN  # Replace with your PUBG API key
    now = datetime.utcnow()
    one_day_ago = now - timedelta(days=1)

    # Convert datetime objects to ISO 8601 formatted strings
    start_time = one_day_ago.isoformat() + "Z"
    end_time = now.isoformat() + "Z"


    for player_name in player_names:
        discord_message = ""
        url = f"https://api.pubg.com/shards/steam/players?filter[playerNames]={player_name}"
        headers = {
            "Authorization": f"Bearer {api_key}",
            "Accept": "application/vnd.api+json"
        }

        url = f"https://api.pubg.com/shards/steam/players?filter[playerNames]={player_name}"
        headers = {
            "Authorization": f"Bearer {api_key}",
            "Accept": "application/vnd.api+json"
        }
        response = requests.get(url, headers=headers)
        if response.status_code == 200:
            player_data = response.json()
            matches = player_data['data'][0]['relationships']['matches']['data']
            matches_data = []
            max = len(matches)
            counter = 0
            for match in matches:
                print(f"{counter}/{max}")
                counter += 1
                match_id = match['id']
                match_data = fetch_match_data(match_id)
                time.sleep(10)
                if match_data:
                    attributes = match_data['data']['attributes']
                    match_start_time = attributes['createdAt']
                    print(match_start_time)
                    # Check if match start time is within the specified timeframe
                    if start_time >= match_start_time:
                        break
                    if start_time <= match_start_time <= end_time:
                        print(attributes['gameMode'])
                        if attributes['gameMode'] != "tdm":
                            matches_data.append(match_data)
                else:
                    print(f"Failed to fetch match data for match ID {match_id}")

            if matches_data:
                stats = calculate_stats(matches_data, player_name)
                discord_message += f"Statistics for matches [**{stats['total_matches']}**] played by {player_name} in the last 24 hours:\n"
                discord_message += f"Average Damage Dealt: {stats['average_damaged']}\n"
                discord_message += f"Average Boosts Used: {stats['average_boosts']}\n"
                discord_message += f"Average Headshots: {stats['average_headshots']}\n"
                discord_message += f"Average Revives: {stats['average_revives']}\n"
                discord_message += f"Average Downed: {stats['average_downed']}\n"
                discord_message += f"Average Time Survived: {stats['average_time_survived']}\n"
                discord_message += f"Average Ride Distance: {stats['average_ride_distance']} metres\n"
                discord_message += f"Average Walk Distance: {stats['average_walk_distance']} metres\n"
                discord_message += f"Longest Kill: {stats['max_longest_kill']} metres\n"
                discord_message += f"Average Finishing Position: {stats['average_win_place']}\n"
                discord_message += f"Chicken Dinners: {stats['wins']} [{round(stats['wins'] / stats['total_matches'] * 100, 3)}%]"
                #send_discord_message.send_discord_message(discord_message)
            else:
                print("No matches played by Temetias in the last 24 hours.")
        else:
            print(f"Failed to fetch PUBG data for player {player_name}. Status code: {response.status_code}")

if __name__ == "__main__":
    main()
