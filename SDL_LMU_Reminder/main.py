import send_discord_message
import random

# List of variations of the message
messages = [
    "Hey there, Kevin! Trust you're doing great. I've been pondering that Le Mans Ultimate Key you mentioned recently. Any chance I could get my hands on it soon? Can't wait to give it a whirl! Appreciate it!",
    "Hi Kevin, I hope everything's going smoothly for you. I've had the Le Mans Ultimate Key on my mind lately. Would it be possible to pick it up from you sometime soon? Really looking forward to trying it out! Thanks a ton!",
    "Hello Kevin! I trust you're having a fantastic day. I've been thinking about that Le Mans Ultimate Key you mentioned before. Is there any chance I could get it from you soon? Eager to give it a go! Thanks a bunch!",
    "Hey Kevin, how's it going? I've been thinking about that Le Mans Ultimate Key you talked about. Any chance I could snag it from you soon? Can't wait to see what it's all about! Thanks a million!",
    "Hi Kevin, I hope you're doing well. I've been mulling over that Le Mans Ultimate Key you mentioned. Would it be possible for me to get it from you soon? Really excited to try it out! Thanks a bunch!",
    "Hey Kevin! Hope everything's going smoothly for you. I've had that Le Mans Ultimate Key on my mind lately. Any chance I could grab it from you soon? Excited to check it out! Thanks a lot!",
    "Hello Kevin, I trust you're doing well. I've been thinking about the Le Mans Ultimate Key you mentioned. Is there any chance I could pick it up from you soon? Can't wait to give it a try! Thanks so much!",
    "Hey Kevin, I hope you're having a great day. I've been pondering that Le Mans Ultimate Key you mentioned. Would it be possible for me to grab it from you soon? Really looking forward to testing it out! Thanks a ton!",
    "Hi Kevin, I trust you're doing well. I've been reminiscing about the Le Mans Ultimate Key you mentioned. Any chance I could grab it from you soon? Can't wait to check it out! Thanks a million!",
    "Hey Kevin! Hope you're doing well. I've been thinking about that Le Mans Ultimate Key you mentioned. Any chance I could pick it up from you soon? Excited to give it a whirl! Thanks a bunch!"
    "Howdy there, Kevin! Sure hope y'all are havin' a mighty fine day! Been thinkin' 'bout that there Le Mans Ultimate Key ya mentioned earlier. Reckon there's any chance I could snag it from ya soon? Dang excited to give it a whirl! Much obliged, partner!"
    "Hail, Kevin! May the gods smile upon thee this day. My musings have led me to contemplate the fabled Le Mans Ultimate Key thou didst mention. Pray, might I procure it from thee with haste? Verily, I am eager to put it to the test! I offer thee my sincerest gratitude!"
    "Ahoy there, Kevin! Avast ye! I be hopin' yer day be filled with plunder and adventure on the high seas! Me thoughts have been sailin' towards that legendary Le Mans Ultimate Key ye spoke of. Be there a chance ye could hand it over to me soon? I be itchin' to set sail with it! Many thanks, matey!"
]


# Select a random message
random_message = random.choice(messages)

# Print the randomly selected message
print(random_message)


send_discord_message.send_discord_message(f"<@135822938439024640> \n`{random_message}`")
print("Successfully ran script.")