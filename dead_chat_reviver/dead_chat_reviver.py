import os
import requests
import datetime
import random
import json
from dotenv import load_dotenv


# Load variables from .env file
load_dotenv()

# Get the Discord token from environment variables
discord_token = os.getenv("discord_token")

DISCORD_API = "https://discord.com/api"

# Replace with your channel ID and bot token
DISCORD_CHANNEL_ID = "1240300121254531072"
TOKEN = discord_token
HOURS_THRESHOLD = 1  # Replace with your desired inactivity duration
STARTERS_FILE = "message_starter.json"  # Path to the JSON file

# Load conversation starters from the JSON file
def load_conversation_starters(file_path):
    try:
        with open(file_path, "r") as file:
            data = json.load(file)
            return data.get("conversation_starters", [])
    except Exception as e:
        print(f"Error loading JSON file: {e}")
        return []

def check_and_send_message(channel_id, hours_threshold, starters_file, token = TOKEN):
    conversation_starters = load_conversation_starters(starters_file)
    if not conversation_starters:
        print("No conversation starters available. Aborting.")
        return

    url = f"https://discord.com/api/v9/channels/{channel_id}/messages?limit=1"
    headers = {
            "Authorization": token,
            "Content-Type": "application/json"
            }

    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        messages = response.json()
        if messages:
            last_message = messages[0]
            timestamp = last_message["timestamp"]
            print(timestamp)

            # Normalize the timestamp
            #timestamp = timestamp.replace("+00:0", "+00:00")
            try:
                last_time = datetime.datetime.fromisoformat(timestamp)
            except ValueError as e:
                print(f"Error parsing timestamp: {e}")
                return

            current_time = datetime.datetime.now(datetime.timezone.utc)            
            elapsed = (current_time - last_time).total_seconds() / 3600  # Convert to hours

            if elapsed > hours_threshold:
                starter = random.choice(conversation_starters)
                send_discord_message(channel_id, starter)
                print(f"Message sent: {starter}")
            else:
                print(f"Last message was only {elapsed:.2f} hours ago. No message sent.")
        else:
            print("No messages found in the channel. Sending a starter.")
            starter = random.choice(conversation_starters)
            send_discord_message(channel_id, starter)
            print(f"Message sent: {starter}")
    else:
        print(f"Failed to fetch messages: {response.status_code} - {response.text}")



def send_discord_message(channel_id, content, token = TOKEN):
    url = f"{DISCORD_API}/v9/channels/{channel_id}/messages"
    headers = {
            "Authorization": token,
            "Content-Type": "application/json"
            }

    data = { "content": str(content) }

    response = requests.post(url, headers=headers, data=json.dumps(data))

    if response.status_code == 200:
        print("Request successful!")
    else:
        print("Request failed with status code:", response.status_code, response.text)

# Run the check
check_and_send_message(DISCORD_CHANNEL_ID, HOURS_THRESHOLD, STARTERS_FILE)