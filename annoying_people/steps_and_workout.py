import send_discord_message
import time

STEPS = 1294987853482164315
WORKOUT = 1294987924076625960

PST_GENERAL = "1015601499117727856"

message1 = f"Hey <@&{STEPS}>, how many steps have we all done in the last week? Can any one be more active than the mighty Ryan Cooper?"
message2 = f"Hey <@&{WORKOUT}>, how have your workouts been this past week? Any challenges? Have we done 150 Minutes of activity? 300 Minutes would be good."

send_discord_message.send_discord_message(message1, PST_GENERAL)
time.sleep(30*60)
send_discord_message.send_discord_message(message2, PST_GENERAL)