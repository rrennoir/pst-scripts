from datetime import datetime
import send_discord_message
import random


messages = [
    "Just X days to go until the Albert Park circuit roars back to life for the season opener.",
    "The wait is almost over - X days until we see Lewis Midfielton in Ferrari red taking on the grid on Melbourne.",
    "X days until the first sparks fly in the 2025 Formula 1 season under the Aussie sun.",
    "Anticipation is building - X days until Maxipad begins his quest for a fifth consecutive title!",
    "Get ready for drama - X days until Kimi Antonelli makes his Mercedes debut down under alongside Osama Bin-Russell.",
    "Australia, are you ready? in X days, Formula 1 is back, and so is the action at Albert Park.",
    "Melbourne will set the stage in X days - who will claim the first victory of 2025?",
    "Countdown to lights out: X days until Broccoli Man and Pastry, and the pack battle it out on home soil!",
    "Excitement is brewing - X days to go before Alonso takes on the new generation in Melbourne. (**The window licker** will be there as well alongside him)",
    "X days until the engines fire up, the tyres hit the track and the race for the 2025 championship begins!"
]

# Set the election date
race_date = datetime(2025, 3, 16)

def countdown():
    today = datetime.now()
    days_remaining = (race_date - today).days + 1

    if days_remaining >= 0:
        # Construct the message with the mention
        message = random.choice(messages)
        message = message.replace("X days", f"{days_remaining} days")
        # Send the message
        print(message)
        send_discord_message.send_discord_message(message, 1015601499117727856)
  
# Schedule this function to run daily as needed
countdown()
