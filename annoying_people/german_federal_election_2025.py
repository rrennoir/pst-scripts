from datetime import datetime
import send_discord_message

# Set the election date
election_date = datetime(2025, 2, 23)

def countdown():
    today = datetime.now()
    days_remaining = (election_date - today).days + 1

    if days_remaining >= 0:
        # Construct the message with the mention
        message = f"📅 Countdown zur Thronbesteigung: Noch {days_remaining} Tage bis König Friedrich I., Erz-Druide Habeck oder der ewige Scholz wieder den Hut aufhaben! 👑🎩🌻"
        # Send the message
        print(message)
        send_discord_message.send_discord_message(message, 1248362414718583004)
    elif days_remaining == 0:
        # Construct the message with the mention
        message = f"📅 Es ist soweit: Deutschland wählt! Möge der Beste gewinnen ... oder der, der die wenigsten Fehler macht. 🗳️🇩🇪"
        # Send the message
        print(message)
        send_discord_message.send_discord_message(message, 1248362414718583004)
    else:
        # Construct the message with the mention
        message = f"📅 Tag {days_remaining} seit der Wahl: Die Koalitionsverhandlungen dauern länger als ein BER-Bauprojekt. 🛠️🗳️"
        # Send the message
        print(message)
        send_discord_message.send_discord_message(message, 1248362414718583004)

# Schedule this function to run daily as needed
countdown()
