from datetime import datetime, timezone, timedelta
import lfm_top10_weekly

def send_discord_message(content, token: str = config.discord_token):
    url = f"{DISCORD_API}/v9/channels/{PST_CHANNEL_ID}/messages"
    headers = {
            "Authorization": token,
            "Content-Type": "application/json"
            }

    data = { "content": str(content) }

    response = requests.post(url, headers=headers, data=json.dumps(data))

    if response.status_code == 200:
        print("Request successful!")
    else:
        print("Request failed with status code:", response.status_code, response.text)

def execute():
    user_list = {"Kylooooo": "83552350525980672", "sovietnugget": "412694491372322818", "emperalhero": "205991837188489216"}
    for user, user_id in user_list:
        top10 = lfm_top10_weekly.get_top10(lfm_top10_weekly.get_top("artist", datetime.now() - timedelta(days=(datetime.now().weekday()) % 7), user))
        top100 = lfm_top10_weekly.get_top("artist", datetime.now() - timedelta(days=(datetime.now().weekday()) % 7) - timedelta(days=7), user)
        embed = lfm_top10_weekly.create_top10_embed(user_id, top10, top100)
        send_discord_message(embed=embed)