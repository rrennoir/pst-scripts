import csv

# Function to process the original CSV file and create a new one
def process_data(input_file, output_file):
    with open(input_file, mode='r') as infile, open(output_file, mode='w', newline='') as outfile:
        reader = csv.reader(infile)
        writer = csv.writer(outfile)

        # Write the new headers
        writer.writerow(["name", "birthday", "talent", "user_id"])

        # Skip the header row in the input file
        next(reader)

        for row in reader:
            # Extract required fields and strip whitespace
            name = row[0].strip()  # Remove leading/trailing spaces from name
            birthday = row[2].strip()  # Remove leading/trailing spaces from birthday
            talent = row[4].strip()  # Remove leading/trailing spaces from talent

            # Write the new row with user_id left blank
            writer.writerow([name, birthday, talent, ""])  # Blank user_id

# Example usage
input_file = 'data.csv'
output_file = 'filtered_data.csv'

process_data(input_file, output_file)

print(f"Data processed and saved to {output_file}")
