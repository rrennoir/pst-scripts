#!/bin/bash

export PY_PYTHON=3.12
alias python='python3.12'
alias py='python3.12'

cd /home/luca/pst-scripts/lfm_driver_market_value || { echo "Failed to change directory"; exit 1; }
pwd >> /home/luca/pst-scripts/lfm_driver_market_value.log
echo $PWD

python3 /home/luca/pst-scripts/lfm_driver_market_value/main.py > lfm_driver_market_value.log
current_date=$(date +"%Y-%m-%d %H:%M:%S")
git add scores.csv
git commit -m "new driver market value data [$current_date]"
git push
