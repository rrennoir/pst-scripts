import csv
import os
import calc
import pandas as pd
import send_discord_message
import time

# Function to create the CSV file with headers if it doesn't exist
def create_csv(file_path):
    # Check if the file already exists to avoid overwriting
    if not os.path.exists(file_path):
        # Create the file and write headers
        with open(file_path, mode='w', newline='') as file:
            writer = csv.writer(file)
            # Add headers
            writer.writerow(["name", "score", "LFM Champions Trophies", "Pro Series Titles Variable", "Podiums", "Poles", "Wins", "Races", "Elo", "Age"])
            print(f"CSV file created with headers at {file_path}")


# Function to append data to the CSV file
def append_data_to_csv(file_path, name, score, lfm_trophies_P1, pro_series_titles, podiums, poles, wins, races, elo, age):
    with open(file_path, mode='a', newline='') as file:
        writer = csv.writer(file)
        # Append the row (name, score)
        writer.writerow([name, score, lfm_trophies_P1, pro_series_titles, podiums, poles, wins, races, elo, age])
        print(f"Added: {name}, {score}")

        

def fetch_data_from_csv(target_value, file_path = "scores.csv", column_name = "name"):
    # Read the CSV file into a pandas DataFrame
    df = pd.read_csv(file_path)

    # Filter the DataFrame based on the column value
    filtered_data = df[df[column_name] == target_value]

    return filtered_data


# Example usage
file_path = 'scores_new.csv'

# Create the CSV file with headers if it doesn't exist
create_csv(file_path)

df = pd.read_csv("driver_data.csv")  # Load CSV into DataFrame
message = ""
current_time = int(time.time())

    
    # Iterate through each row
for index, row in df.iterrows():
    value, lfm_trophies_P1, pro_series_titles, podiums, poles, wins, races, elo, age = calc.calc(row)
    name = row['name']
    try:
        old_value = fetch_data_from_csv(name)['score'].values[0]
    except:
        old_value = 0

    if old_value > value:
        message += (f"- Driver Value for {name} changed from {old_value} to {value} [{value-old_value}]\n")
    elif value > old_value:
        message += (f"+ Driver Value for {name} changed from {old_value} to {value} [+{value-old_value}]\n")
    else:
        print(f"Driver Value for {name} did not change")

    # Append data to the CSV file
    append_data_to_csv(file_path, row['name'], value, lfm_trophies_P1, pro_series_titles, podiums, poles, wins, races, elo, age)

if message:
    message_id = send_discord_message.send_discord_message(f"Updated Market Values as of <t:{current_time}>\n```diff\n{message}```")
    send_discord_message.send_crosspost(message_id)
os.remove("scores.csv")
os.rename(file_path, "scores.csv")
